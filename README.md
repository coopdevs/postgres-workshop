These are the Ansible scripts used to provison the `pgworkshop.coopdevs.org` server, which will be used for the PostgreSQL Workshop held on 06/06/2019 at TecnoCampus.

You'll find the presentation and guidelines of the session at https://gitlab.com/coopdevs/postgresql-workshop-talk.

## Usage

This sets up a server with a database called `openfoodnetwork` with N schemas and users following the `userXX` pattern.

To provision more users simply append them in `pg_workshop_users` and execute the `provision.yml` playbook again.

## How to connect as student

Simply open a `psql` session against the workshop's server using

```sh
$ psql -h pgworkshop.coopdevs.org openfoodnetwork user01
```

Note the first argument is the host, the second one the database and the third the user.

You will be asked for the password. Ask the teacher for it.

## Access all schemas

To do that you need to access as the `postgres` user. You can do so as follows.

```sh
$ ssh pgworkshop.coopdevs.org
$ sudo su postgres
$ psql
```

Note this relies on you having access to the server using the same username of your local machine.

You can now run commands such as

```
postgres=# \dn
  List of schemas
  Name  |  Owner   
--------+----------
 public | postgres
 user01 | user01
 user02 | user02
(3 rows)
```

## Grant SELECT access to the public schema

Connect to the `openfoodnetwork` database as `postgres` and execute

```
GRANT SELECT ON ALL TABLES IN SCHEMA public TO user01;
```

## Import a production dump

Make sure references to `pg_stat_activity_dd` and its function are removed as this won't work and make the user superuser as follows

```
postgres=# alter role ofn_user with superuser;
```

Then, you can import the dump

```
psql -h pgworkshop.local openfoodnetwork ofn_user -v ON_ERROR_STOP=1 < production_dump.sql
```

## Recreate dev environment

Want a start from scratch? execute the following commands

```sh
sudo lxc-stop -n pgworkshop
sudo lxc-destroy pgworkshop
devenv
ansible-playbook playbooks/sys_admins.yml --limit=dev
ansible-playbook playbooks/provision.yml --limit=dev
```
